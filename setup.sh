# Enter username@machine where machine has access to /acc, /nfs, /mcr and /user.
export VM=emthalle@cwe-513-vpl113.cern.ch

# Match FESA and makefile versions via /acc/local/share/fesa/fesa-fwk/$FESA_VERSION/fesa-makefile/Make.parent.
export FETCH_FESA=1
export FESA_VERSION=8.1.0
export FESA_MAKEFILE_VERSION=4.0.0
export MAKEFILE_GENERIC_VERSION=3.0.0

export FETCH_HARDWARE_SIM=1

# Match FESA and boost version via https://wikis.cern.ch/display/FESA3/Release+Info+Framework.
export FETCH_BOOST=1
export BOOST_VERSION=1.69.0

export FETCH_GOOGLE=1
export GTEST_VERSION=1.10.0
export SNAPPY_VERSION=1.1.7

export FETCH_DRV=1
export CTR_VERSION=2.2
export EDGE_VERSION=2.0.0
export IPOCTAL_VERSION=1.0
export MASTERFIP_VERSION=1.1.3
export MOCKTURTLE_VERSION=3.0.0

export FETCH_TIMDT=0
export TIMDT_VERSION=PRO

export FETCH_CMW=0
export CMW_VERSION=5.1.1
export CMW_DIR_VERSION=4.1.1

export FETCH_PM=0
export PM_VERSION=1.0.9

export FETCH_ACCPY=0

# $1=src, $2=dst, $3=transform_symlink_into_referent
fetch_remote() {
  echo Fetching $1...

  mkdir -p $(dirname $2)

  if [[ $3 ]]; then
    # Passing -L makes rsync replace symlinks with their destinations.
    symlink_option=-L
  else
    symlink_option=-l
  fi

  # Passing -a makes rsync keep the permissions, owner, group, ... intact.
  rsync -a $symlink_option $VM:$1 $2
}

# /acc/.local/share/
fetch_remote /acc/.local/share/fesa/fesa-plugin-commandline-cern/PRO/ acc/.local/share/fesa/fesa-plugin-commandline-cern/PRO/

# /acc/local/Linux/
fetch_remote /acc/local/Linux/bin/ acc/local/Linux/bin/ 1
fetch_remote /acc/local/Linux/x86_64-linux-gcc/current/Make.variables acc/local/Linux/x86_64-linux-gcc/current/Make.variables
fetch_remote /acc/local/Linux/x86_64-linux-gcc/current/gcc/ acc/local/Linux/x86_64-linux-gcc/current/gcc/ 1

# /acc/src
fetch_remote /acc/dsc/src/co/Make.auto acc/dsc/src/co/Make.auto
fetch_remote /acc/dsc/src/co/Make.common acc/dsc/src/co/Make.common
fetch_remote /acc/src/dsc/co/Make.auto acc/src/dsc/co/Make.auto
fetch_remote /acc/src/dsc/co/Make.common acc/src/dsc/co/Make.common

# /acc/sys/
fetch_remote /acc/sys/cdk/L867/ acc/sys/cdk/L867/
fetch_remote /acc/sys/Linux/toolchain_libs/ acc/sys/Linux/toolchain_libs/
ln -s / acc/sys/L867

# /etc/
fetch_remote /etc/pki/rpm-gpg/ etc/pki/rpm-gpg/
fetch_remote /etc/yum.conf etc/yum.conf
fetch_remote /etc/yum.repos.d/ etc/yum.repos.d/
fetch_remote /etc/yum/ etc/yum/

# /mcr/
fetch_remote /mcr/bin/ mcr/bin/

# /user/
fetch_remote /user/pcrops/deployments/applications/cern/fesa/Navigator/NEXT/ user/pcrops/deployments/applications/cern/fesa/Navigator/NEXT/ 1
fetch_remote /user/rbac/pkey/ user/rbac/pkey/

if [[ $FETCH_FESA == 1 ]]; then
  fetch_remote /acc/local/L867/fesa/fesa-fwk/$FESA_VERSION/ acc/local/L867/fesa/fesa-fwk/$FESA_VERSION/ 1
  fetch_remote /nfs/cs-ccr-felab/dsc/tst/data/fesa/fesa-fwk/$FESA_VERSION/ dsc/data/fesa/fesa-fwk/$FESA_VERSION/
  fetch_remote /acc/local/share/fesa/fesa-cfg/ acc/local/share/fesa/fesa-cfg/ 1
  fetch_remote /acc/local/share/fesa/fesa-codegen-cern/ acc/local/share/fesa/fesa-codegen-cern/ 1
  fetch_remote /acc/local/share/fesa/fesa-codegen-standalone-cern/ acc/local/share/fesa/fesa-codegen-standalone-cern/ 1
  fetch_remote /acc/local/share/fesa/fesa-codegen-standalone/ acc/local/share/fesa/fesa-codegen-standalone/ 1
  fetch_remote /acc/local/share/fesa/fesa-codegen/ acc/local/share/fesa/fesa-codegen/ 1
  fetch_remote /acc/local/share/fesa/fesa-fwk/$FESA_VERSION/ acc/local/share/fesa/fesa-fwk/$FESA_VERSION/
  fetch_remote /acc/local/share/fesa/fesa-fwk/$FESA_VERSION/ acc/local/share/fesa/fesa-fwk/$FESA_VERSION/ 1
  fetch_remote /acc/local/share/fesa/fesa-makefile/$FESA_MAKEFILE_VERSION/ acc/local/share/fesa/fesa-makefile/$FESA_MAKEFILE_VERSION/
  fetch_remote /acc/local/share/fesa/tools/ acc/local/share/fesa/tools/
  fetch_remote /acc/local/share/fesa/tools/runtime/ acc/local/share/fesa/tools/runtime/
  fetch_remote /acc/local/share/makefile-generic/$MAKEFILE_GENERIC_VERSION/ acc/local/share/makefile-generic/$MAKEFILE_GENERIC_VERSION/
fi

if [[ $FETCH_BOOST == 1 ]]; then
  fetch_remote /acc/local/L867/3rdparty/boost/$BOOST_VERSION/ acc/local/L867/3rdparty/boost/$BOOST_VERSION/
fi

if [[ $FETCH_GOOGLE == 1 ]]; then
  fetch_remote /acc/local/L867/3rdparty/google/gmock/$GTEST_VERSION/ acc/local/L867/3rdparty/google/gmock/$GTEST_VERSION/
  fetch_remote /acc/local/L867/3rdparty/google/gtest/$GTEST_VERSION/ acc/local/L867/3rdparty/google/gtest/$GTEST_VERSION/
  fetch_remote /acc/local/L867/3rdparty/google/snappy/$SNAPPY_VERSION/ acc/local/L867/3rdparty/google/snappy/$SNAPPY_VERSION/
fi

if [[ $FETCH_DRV == 1 ]]; then
  fetch_remote /acc/local/L867/drv/ctr/$CTR_VERSION/ acc/local/L867/drv/ctr/$CTR_VERSION/
  fetch_remote /acc/local/L867/drv/edge/$EDGE_VERSION/ acc/local/L867/drv/edge/$EDGE_VERSION/
  fetch_remote /acc/local/L867/drv/ipoctal/$IPOCTAL_VERSION/ acc/local/L867/drv/ipoctal/$IPOCTAL_VERSION/
  fetch_remote /acc/local/L867/drv/masterfip/$MASTERFIP_VERSION/ acc/local/L867/drv/masterfip/$MASTERFIP_VERSION/
  fetch_remote /acc/local/L867/drv/mockturtle/$MOCKTURTLE_VERSION/ acc/local/L867/drv/mockturtle/$MOCKTURTLE_VERSION/
  fetch_remote /acc/local/L867/gm/ acc/local/L867/gm/
  fetch_remote /acc/local/share/edge/ acc/local/share/edge/
  fetch_remote /acc/local/share/gm/ acc/local/share/gm/
fi

if [[ $FETCH_TIMDT == 1 ]]; then
  fetch_remote /acc/local/L867/timdt/timdt-fwk/$TIMDT_VERSION/ acc/local/L867/timdt/timdt-fwk/$TIMDT_VERSION/
fi

if [[ $FETCH_CMW == 1 ]]; then
  fetch_remote /acc/local/L867/cmw/cmw-directory-client/$CMW_DIR_VERSION/ acc/local/L867/cmw/cmw-directory-client/$CMW_DIR_VERSION/
  fetch_remote /acc/local/L867/cmw/cmw-rda3/$CMW_VERSION/ acc/local/L867/cmw/cmw-rda3/$CMW_VERSION/ 1
fi

if [[ $FETCH_PM == 1 ]]; then
  fetch_remote /acc/local/L867/pm/pm-dc-rda3-lib/$PM_VERSION/ acc/local/L867/pm/pm-dc-rda3-lib/$PM_VERSION/
fi

if [[ $FETCH_HARDWARE_SIM == 1 ]]; then
  fetch_remote /user/mpeops/cpp/simulation/ user/mpeops/cpp/simulation/
fi

if [[ $FETCH_ACCPY == 1 ]]; then
  fetch_remote /acc/local/share/python/acc-py/ acc/local/share/python/acc-py/
fi
