from gitlab-registry.cern.ch/acc-co/docker/acc-base-images/acc_cc7_jdk8

# Justifications for each dependency
#
# boost169-python3-devel : * remove if /acc/local/L867/3rdparty/boost/1.69.0/lib/libboost_1_69_0_python36.so exists *
#                          Dependency of TimSim
# curl-devel             : Provides libcurl which is used by FESA build
# doxygen                : Used in FESA CLI tools
# gcc-c++                : Legacy C++ compiler
# git                    : For convenience
# gtk3                   : Dependency of Eclipse
# less                   : For convenience
# libxml2-devel          : Used in FESA CLI tools
# libxml2-python         : Used in FESA CLI tools
# make                   : Required by FESA build
# svn                    : Required to checkout a FESA project
# openssh-clients        : Required to e.g. `svn co svn+ssh://...`
# openssl-devel          : Provides libcrypto which is used by FESA build
# perl-XML-Twig          : Provides xml_grep which is used by FESA build
# python36-devel         : Required to build extra/timsim
# rsync                  : Required to deploy extra/timsim and extra/edgesim
# shadow-utils           : Provides useradd which is used to set up $HOST_USER in the container
# sudo                   : Generally useful, e.g. to run FESA classes while working under $HOST_USER
# vim-common             : Provides xxd which is used in DQAmx make files
# wget                   : Required by FESA Navigator
# wmctrl                 : Required by FESA Navigator, used (but not required) by jws to check if the app window appeared
#
# Keep dependencies sorted alphabetically
RUN yum -y install \
boost169-python3-devel \
curl-devel \
doxygen \
gcc-c++ \
git \
gtk3 \
less \
libxml2-devel \
libxml2-python \
make \
svn \
openssh-clients \
openssl-devel \
perl-XML-Twig \
python36-devel \
rsync \
shadow-utils \
sudo \
vim \
vim-common \
wget \
wmctrl \
zsh \
&& yum -y clean all && rm -fr /var/cache

# Required for FESA navigator to start
RUN mkdir -p /local/java.users/.java

# User config to allow working with the user's files (e.g. existing checked out projects on the host FS)
ARG HOST_USER
ENV HOST_USER=$HOST_USER

ARG HOST_UID
ENV HOST_UID=$HOST_UID

RUN useradd -u $HOST_UID $HOST_USER && \
echo "$HOST_USER"':cern' | chpasswd && \
usermod -aG wheel $HOST_USER && \
mkdir /home/$HOST_USER/.ssh && \
chown -R ${HOST_USER}:${HOST_USER} /home/$HOST_USER/

COPY ./id_dsa /home/$HOST_USER/.ssh/
COPY ./ssh_config /home/$HOST_USER/.ssh/config

USER $HOST_UID

ARG DISPLAY
ENV DISPLAY=$DISPLAY

# local dir copied from VM, mounted by docker-compose.yml
ENV PATH "$PATH:/acc/local/Linux/bin"

RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" --unattended
CMD ["zsh"]
