# fesa-local-dev-env

## Introduction
This is a project to enable FESA development on any machine running docker.  
It sets up a container based on cern/cc7-base (L867) with partial copies of various CERN filesystems (/acc, /nfs, /mcr, /user).  
X11 clients inside the container can communicate with the X11 server on the host machine. Therefore it is possible to run graphical applications like Accsoft Eclipse Pro.

## How to run
Edit the parameters `VM`, `*_VERSION` in setup.sh.

### 1) Run setup.sh

```
./setup.sh
```

### 2) Build the container

Optional arguments:    
* `HOST_USER=...` - Appear as `$HOST_USER` inside the container.
```
HOST_UID=$UID docker-compose build app
```

### 3) Run the container 

Optional arguments:  
* `HOSTNAME=...` - Set container hostname.  
* `MNT_{1, 2, 3, 4}=...` - Mount path from host into container. Read Write.  
* `MNT_RO_{1, 2}=...` - Mount path from host into container. Read Only.
```
docker-compose run app
```

**The password of the user is "`cern`".**

### Example

```
./setup.sh
HOST_UID=$UID docker-compose build app
HOSTNAME=pcte214617 MNT_1=~/Accsoft-Eclipse-Pro MNT_2=~/Workspaces docker-compose run app
```
On success you will be greeted by a shell in the L867 container.  

## Timing Simulation

FESA classes that have actions mapped to timing events will, by default, not run inside the container: `Timing: Library not initialized in function <Init>`.  
To run these FESA classes either all timing events have to be unmapped or timing has to be simulated.


To use timing simulation, patch `Makefile.specific` of the deploy unit like so:
```
ifeq ($(SIMULATE_HARDWARE), true)
    TIMDT_LIB_CPP_SIM_PATH := /user/mpeops/cpp/simulation/timdt/timdt-lib-cpp-sim/$(FESAFWK_VERSION).0/lib/
    LINKER_FLAGS := -L$(TIMDT_LIB_CPP_SIM_PATH) -Wl,-rpath=$(TIMDT_LIB_CPP_SIM_PATH) -ltimdt-lib-cpp-sim $(LINKER_FLAGS)
endif
``` 

The timing events are scheduled via a user provided python module (version 3.6).  
The module must define a function named `run` with 4 parameters: `event_name, event_user, event_domain, timestamp`.  The unit of `timestamp` is nanoseconds since epoch.  
The function is expected to return a list of 4-tuples identifying the events to schedule next.  
The very first time the function is called, the parameters `event_*` will be the empty string and `timestamp` is the current time.   
The path to the python module is read from a environment variable with name `TIMSIM_MODULE`.

Example module:
```
def run(event_name, event_user, event_domain, timestamp):
    return [('BX.SCY-CT', 'Emanuel', 'FCC', timestamp + 1000000000)]
```
This module schedules `BX.SCY-CT` every second.

Example module 2:
```
intervals = {'BX.SCY-CT': 1000000000, 'SX.AWAKE-CW-CT': 500000000}


def run(event_name, event_user, event_domain, timestamp):
    if len(event_name) == 0:
        # Initialize the event queue with 2 events.
        event_a = ('BX.SCY-CT', 'Emanuel', 'FCC',
                   timestamp + intervals['BX.SCY-CT'])
        event_b = ('SX.AWAKE-CW-CT', 'Emanuel', 'FCC',
                   timestamp + intervals['SX.AWAKE-CW-CT'])

        return [event_a, event_b]

    # Simply re-schedule the same event to make it cyclic.
    return [(event_name, event_user, event_domain,
             timestamp + intervals[event_name])]
```
This module schedules `BX.SCY-CT` every second and `SX.AWAKE-CW-CT` every 0.5 seconds..



Executing the deploy unit with simulated timing:
```
TIMSIM_MODULE=/home/emanuel/simple_timing_schedule.py ./run_M.sh
```
Note: Currently this does command will not work as expected because `run_M.sh` does not preserve the environment variables.  
To preserve environment variables, open `/acc/local/share/fesa/tools/runtime/run.py` and replace `process = subprocess.Popen([ "sudo", command ] + args, stdout = subprocess.PIPE)` with `process = subprocess.Popen([ "sudo", "-E", command ] + args, stdout = subprocess.PIPE)`.


## Known Issues

* FESA Navigator takes 10 minutes to start up due to a broken connection.  
Note: This only happens if the deploy unit is not running, if the deploy unit is running FESA Navigator starts up right away.
* Running the deploy unit via the run button in eclipse does not work because it attempts to mount the project folder with `mount -t nfs ...`
* `run_M.sh` sometimes freezes when trying to terminate it via SIGINT.
