#include <edge/libedge.h>

#include <dlfcn.h>

#include <atomic>
#include <cctype>
#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <future>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

#include <boost/algorithm/string/case_conv.hpp>

namespace {

const char* CISX_EDGE_DIR = "/user/mpeops/cpp/simulation/edge/CISX/";
const char* CIB3_EDGE_DIR = "/user/mpeops/cpp/simulation/edge/CIB3/";
const char* CISV_EDGE_DIR = "/user/mpeops/cpp/simulation/edge/CISV/";
const char* UNITTEST_EDGE_DIR = "/user/mpeops/cpp/simulation/edge/UNITTEST/";

std::function<decltype(edge_sim_open)> original_edge_sim_open;
std::function<decltype(edge_get_reg_by_names)> original_edge_get_reg_by_names;
std::function<decltype(edge_get)> original_edge_get;
std::function<decltype(edge_set)> original_edge_set;
std::function<decltype(edge_get_range)> original_edge_get_range;
std::function<decltype(edge_set_range)> original_edge_set_range;
std::function<decltype(edge_strerror)> original_edge_strerror;
std::function<decltype(edge_close)> original_edge_close;
std::atomic<bool> simulateHistoryBuffer = true;
std::unordered_map<std::string, std::future<void>> historyBufferTasks;
bool initialized = false;
std::mutex mutex;

void initialize() {
	void* libedge = dlopen("/acc/local/L867/drv/edge/2.0.0/lib/libedge.so", RTLD_LAZY | RTLD_LOCAL);

	original_edge_sim_open = reinterpret_cast<decltype(edge_sim_open)*>(dlsym(libedge, "edge_sim_open"));

	original_edge_get_reg_by_names =
		reinterpret_cast<decltype(edge_get_reg_by_names)*>(dlsym(libedge, "edge_get_reg_by_names"));

	original_edge_get = reinterpret_cast<decltype(edge_get)*>(dlsym(libedge, "edge_get"));

	original_edge_get_range = reinterpret_cast<decltype(edge_get_range)*>(dlsym(libedge, "edge_get_range"));

	original_edge_set = reinterpret_cast<decltype(edge_set)*>(dlsym(libedge, "edge_set"));

	original_edge_set_range = reinterpret_cast<decltype(edge_set_range)*>(dlsym(libedge, "edge_set_range"));

	original_edge_strerror = reinterpret_cast<decltype(edge_strerror)*>(dlsym(libedge, "edge_strerror"));

	original_edge_close = reinterpret_cast<decltype(edge_close)*>(dlsym(libedge, "edge_close"));
}

void initializeCisx(struct edge_hdl* hdl, const std::string& driverName) {
	/*
	 * In simulation mode all registers are zeroed.
	 * The minimum we have initialize for this fesa-class is the register
	 * VARIANT_NAME.
	 */

	if (driverName.length() > 7) {
		fprintf(stderr,
				"Edgesim: Attempted to open an Edge driver with a too long name "
				"('%s'). The "
				"Edge driver name is expected to be of the format CIS____, where "
				"____ is the variant name with a length between 1 and 4",
				driverName.c_str());
		exit(1);
	}

	auto variantName = driverName.substr(3);
	char buffer[4] = {};
	int index = 3;
	for (auto c : variantName) {
		buffer[index] = std::toupper(c);
		--index;
	}

	auto* reg = edge_get_reg_by_names(hdl, "ControllerRegs", "VARIANT_NAME");
	edge_set(hdl, reg, buffer, 1);
}

struct HistoryBufferView {
	int32_t seconds;

	unsigned int monitorStatus : 12;
	unsigned int microseconds : 20;

	unsigned int subtype : 8;
	unsigned int type : 6;
	unsigned int visibility : 2;
	unsigned int controlStatus : 16;

	int32_t details;
} __attribute__((packed));

struct HistoryBufferState {
	// HB_LAST_REC, HB_POINTER
	uint32_t last;
	// HB_STATUS, HB_RECORDS_LIMIT
	uint32_t status;
	// HB_ALL_DATA, HB_ADDRESS
	std::vector<HistoryBufferView> data = std::vector<HistoryBufferView>(1024);
};

void startHistoryBufferSimulation(struct edge_hdl* hdl, const std::string& driverName, int lun) {
	auto id = driverName + std::to_string(lun);

	if (historyBufferTasks.count(id) > 0) {
		return;
	}

	std::string lastRegName;
	std::string lastBlockName;
	std::string statusRegName;
	std::string statusBlockName;
	std::string dataRegName;
	std::string dataBlockName;

	if (driverName == "cibm64" || driverName == "cibds64") {
		lastRegName = "HB_LAST_REC";
		lastBlockName = "HistoryBuffer";
		statusRegName = "HB_STATUS";
		statusBlockName = "HistoryBuffer";
		dataRegName = "HB_ALL_DATA";
		dataBlockName = "HistoryBuffer";
	} else if (driverName == "cibg64") {
		lastRegName = "HB_LAST_REC";
		lastBlockName = "StatusReg";
		statusRegName = "HB_STATUS";
		statusBlockName = "StatusReg";
		dataRegName = "HB_ALL_DATA";
		dataBlockName = "HistoryBuffer";
	} else if (driverName.rfind("cis", 0) != std::string::npos) {
		lastRegName = "HB_POINTER";
		lastBlockName = "StatusReg";
		statusRegName = "HB_RECORDS_LIMIT";
		statusBlockName = "StatusReg";
		dataRegName = "HB_ADDRESS";
		dataBlockName = "HistoryBuffer";
	} else {
		fprintf(stderr, "Edgesim: Missing history buffer details for %s\n", driverName.c_str());
		exit(1);
	}

	auto* dataReg = edge_get_reg_by_names(hdl, dataBlockName.c_str(), dataRegName.c_str());
	auto* lastReg = edge_get_reg_by_names(hdl, lastBlockName.c_str(), lastRegName.c_str());
	auto* statusReg = edge_get_reg_by_names(hdl, statusBlockName.c_str(), statusRegName.c_str());

	if (dataReg == nullptr || lastReg == nullptr || statusReg == nullptr) {
		fprintf(stderr, "Edgesim: Error %s\n", edge_strerror(hdl));
		exit(1);
	}

	HistoryBufferState hb;
	hb.status = 0;
	hb.last = 0;
	memset(hb.data.data(), 0, sizeof(HistoryBufferView) * hb.data.size());

	edge_set(hdl, dataReg, hb.data.data(), hb.data.size() * 4);
	edge_set(hdl, lastReg, &hb.last, 1);
	edge_set(hdl, statusReg, &hb.status, 1);

	auto task =
		std::async(std::launch::async, [hdl, dataReg, lastReg, statusReg, driverName, lastRegName, lastBlockName,
										statusRegName, statusBlockName, dataRegName, dataBlockName]() {
			HistoryBufferState hb;
			hb.status = 0;
			hb.last = 0;
			memset(hb.data.data(), 0, sizeof(HistoryBufferView) * hb.data.size());

			int count = 0;
			while (simulateHistoryBuffer) {
				std::this_thread::sleep_for(std::chrono::seconds(1));

				hb.data.at(hb.last).details = count++;
				if (hb.status == 0 && hb.last == (hb.data.size() - 1)) {
					hb.status = 1;
				}
				hb.last = (hb.last + 1) % hb.data.size();

				edge_set(hdl, dataReg, hb.data.data(), hb.data.size() * 4);
				edge_set(hdl, lastReg, &hb.last, 1);
				edge_set(hdl, statusReg, &hb.status, 1);
			}
		});

	historyBufferTasks[id] = std::move(task);
}

}  // namespace

struct edge_hdl* edge_open(const char* drvname, int lun) {
	if (!initialized) {
		initialize();
		initialized = true;
	}
	fprintf(stderr, "Edgesim: Simulating '%s' (lun=%d)...\n", drvname, lun);

	struct edge_hdl* hdl;

	auto driverName = boost::to_lower_copy(std::string(drvname));
	if (driverName.rfind("cis", 0) != std::string::npos) {
		auto driverPath = CISX_EDGE_DIR + driverName;
		hdl = original_edge_sim_open(driverName.c_str(), lun, driverPath.c_str());

		initializeCisx(hdl, driverName);
		startHistoryBufferSimulation(hdl, driverName, lun);
	} else if (driverName.rfind("cib", 0) != std::string::npos) {
		auto driverPath = CIB3_EDGE_DIR + driverName;
		hdl = original_edge_sim_open(driverName.c_str(), lun, driverPath.c_str());

		startHistoryBufferSimulation(hdl, driverName, lun);
	} else if (driverName == "cisv64") {
		auto driverPath = CISV_EDGE_DIR + driverName;
		hdl = original_edge_sim_open(driverName.c_str(), lun, driverPath.c_str());
	} else if (driverName == "unittest_base") {
		auto driverPath = UNITTEST_EDGE_DIR + driverName;
		hdl = original_edge_sim_open(driverName.c_str(), lun, driverPath.c_str());
	} else if (driverName == "unittest_duplicate_register_name") {
		auto driverPath = UNITTEST_EDGE_DIR + driverName;
		hdl = original_edge_sim_open(driverName.c_str(), lun, driverPath.c_str());
	} else {
		fprintf(stderr, "Edgesim: Driver '%s' is not supported.\n", drvname);
		exit(1);
	}

	return hdl;
}

struct edge_reg* edge_get_reg_by_names(struct edge_hdl* h, const char* block_name, const char* reg_name) {
	std::lock_guard<std::mutex> lock(mutex);
	return original_edge_get_reg_by_names(h, block_name, reg_name);
}

int edge_get(struct edge_hdl* hdl, struct edge_reg* reg, void* data, uint32_t nelt) {
	std::lock_guard<std::mutex> lock(mutex);
	return original_edge_get(hdl, reg, data, nelt);
}

int edge_set(struct edge_hdl* hdl, struct edge_reg* reg, void* data, uint32_t nelt) {
	std::lock_guard<std::mutex> lock(mutex);
	return original_edge_set(hdl, reg, data, nelt);
}

int edge_get_range(struct edge_hdl* hdl, struct edge_reg* reg, uint32_t from, void* data, uint32_t nelt) {
	std::lock_guard<std::mutex> lock(mutex);
	return original_edge_get_range(hdl, reg, from, data, nelt);
}

int edge_set_range(struct edge_hdl* hdl, struct edge_reg* reg, uint32_t from, void* data, uint32_t nelt) {
	std::lock_guard<std::mutex> lock(mutex);
	return original_edge_set_range(hdl, reg, from, data, nelt);
}

const char* edge_strerror(struct edge_hdl* hdl) {
	std::lock_guard<std::mutex> lock(mutex);
	return original_edge_strerror(hdl);
}

int edge_close(struct edge_hdl* h) {
	simulateHistoryBuffer = false;
	for (auto& [id, task] : historyBufferTasks) {
		task.wait();
	}

	return original_edge_close(h);
}
