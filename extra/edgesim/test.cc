#include <edge/libedge.h>

#include <unistd.h>
#include <cstdio>

int main(int argc, char* argv[]) {
	auto* hdl = edge_open("cibg64", 0);
	if (hdl == nullptr) {
		fprintf(stderr, "%s\n", edge_strerror(hdl));
	} else {
		fprintf(stderr, "%p\n", hdl);
	}

	auto lastRegName = "HB_LAST_REC";
	auto lastBlockName = "StatusReg";
	auto statusRegName = "HB_STATUS";
	auto statusBlockName = "StatusReg";
	auto dataRegName = "HB_ALL_DATA";
	auto dataBlockName = "HistoryBuffer";

	while (true) {
		auto* last_reg = edge_get_reg_by_names(hdl, lastBlockName, lastRegName);
		auto* status_reg = edge_get_reg_by_names(hdl, statusBlockName, statusRegName);
		auto* data_reg = edge_get_reg_by_names(hdl, dataBlockName, dataRegName);
		uint32_t hb[1024];
		edge_get(hdl, status_reg, hb, 1);
		fprintf(stderr, "%x ", hb[0]);
		edge_get(hdl, last_reg, hb, 1);
		fprintf(stderr, "%x ", hb[0]);
		edge_get(hdl, data_reg, hb, 1024);
		fprintf(stderr, "%x\n", hb[0]);

		sleep(1);
	}

	edge_close(hdl);
}
