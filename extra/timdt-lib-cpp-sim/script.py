intervals = {'BX.SCY-CT': 1000000000, 'SX.AWAKE-CW-CT': 500000000}


def run(event_name, event_user, event_domain, timestamp):
    if len(event_name) == 0:
        # Initialize the event queue with 2 events.
        event_a = ('BX.SCY-CT', 'Emanuel', 'FCC',
                   timestamp + intervals['BX.SCY-CT'])
        event_b = ('SX.AWAKE-CW-CT', 'Emanuel', 'FCC',
                   timestamp + intervals['SX.AWAKE-CW-CT'])

        return [event_a, event_b]

    # Simply re-schedule the same event to make it cyclic.
    return [(event_name, event_user, event_domain,
             timestamp + intervals[event_name])]
