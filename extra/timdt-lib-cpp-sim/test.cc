#include <timdt-lib-cpp/Timing.h>

#include <memory>

int main() {
	Timing::ConnectionContext c;
	c.connect("dummy");
	c.setQueue(true);
	c.setTimeOut(0);

	while (true) {
		std::unique_ptr<Timing::EventValue> eventValue(new Timing::EventValue());
		c.wait(eventValue.get());
		fprintf(stderr, "Received Timing-Event '%s' %ld %ld\n", eventValue->getName().c_str(),
				eventValue->getCycleTimestamp().time.tv_sec, eventValue->getHwTimestamp().time.tv_sec);
	}
}
