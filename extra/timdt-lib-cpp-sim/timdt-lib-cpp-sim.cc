#include <timdt-lib-cpp/ConnectionContext.h>
#include <timdt-lib-cpp/Domain.h>
#include <timdt-lib-cpp/EventDescriptor.h>
#include <timdt-lib-cpp/EventValue.h>
#include <timdt-lib-cpp/Field.h>
#include <timdt-lib-cpp/Value.h>

#include <unistd.h>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <initializer_list>
#include <queue>
#include <string>
#include <thread>
#include <vector>

#include <boost/python.hpp>
#include <boost/python/call.hpp>
#include <boost/python/dict.hpp>
#include <boost/python/errors.hpp>
#include <boost/python/exec.hpp>
#include <boost/python/import.hpp>
#include <boost/python/list.hpp>
#include <boost/python/object_fwd.hpp>

namespace {

int64_t timeNano() {
	auto now = std::chrono::high_resolution_clock::now();
	return std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count();
}

struct TimingEvent {
	std::string name;
	std::string user;
	std::string domain;
	int64_t timestamp;
	tTimingTime hwTimestamp = {};
	tTimingTime cycleTimestamp = {};
};

class TimingEventScheduler {
 public:
	TimingEventScheduler() : queue_(compare) {
		auto path_cstr = std::getenv("TIMSIM_MODULE");
		if (path_cstr == nullptr) {
			return;
		}

		auto path = std::string(path_cstr);
		auto pos = path.rfind('/');
		auto dir = path.substr(0, pos + 1);
		auto module = path.substr(pos + 1, path.rfind('.') - pos - 1);
		const char* scriptTemplate = R"(
import sys
sys.path.append('%s')
import %s
)";
		char script[1024];
		snprintf(script, sizeof(script), scriptTemplate, dir.c_str(), module.c_str());

		Py_Initialize();

		try {
			mainModule_ = boost::python::import("__main__");
			mainNamespace_ = mainModule_.attr("__dict__");
			boost::python::exec(script, mainNamespace_);
			mainFunction_ = mainNamespace_[module].attr("run");
		} catch (const boost::python::error_already_set& e) {
			PyErr_Print();
			throw;
		}

		TimingEvent first;
		first.timestamp = timeNano();
		queue_.push(first);
	}

	TimingEvent wait() {
		if (queue_.empty()) {
			pause();
		}

		auto event = queue_.top();
		queue_.pop();

		event.hwTimestamp.time.tv_sec = event.timestamp / 1'000'000'000LL;
		event.hwTimestamp.time.tv_nsec = event.timestamp % 1'000'000'000LL;
		event.cycleTimestamp = event.hwTimestamp;

		try {
			boost::python::list events = boost::python::call<boost::python::list>(
				mainFunction_.ptr(), event.name, event.user, event.domain, event.timestamp);
			auto len = boost::python::len(events);
			for (int i = 0; i < len; ++i) {
				TimingEvent pyEvent;
				pyEvent.name = boost::python::extract<std::string>(events[i][0]);
				pyEvent.user = boost::python::extract<std::string>(events[i][1]);
				pyEvent.domain = boost::python::extract<std::string>(events[i][2]);
				pyEvent.timestamp = boost::python::extract<int64_t>(events[i][3]);
				queue_.push(pyEvent);
			}
		} catch (const boost::python::error_already_set& e) {
			PyErr_Print();
			throw;
		}

		auto now = timeNano();
		auto dt = event.timestamp - now;
		if (dt > 0) {
			std::this_thread::sleep_for(std::chrono::nanoseconds(dt));
		}

		return event;
	}

 private:
	static bool compare(const TimingEvent& lhs, const TimingEvent& rhs) {
		return lhs.timestamp > rhs.timestamp;
	};

	std::priority_queue<TimingEvent, std::vector<TimingEvent>, decltype(compare)*> queue_;
	boost::python::object mainModule_;
	boost::python::object mainNamespace_;
	boost::python::object mainFunction_;
};

TimingEvent activeEvent;

}  // namespace

namespace Timing {

#if 0  // Enable or disable call tracing.
#define LOG_CALL() fprintf(stderr, "TimSim: %s::%s called\n", typeid(*this).name(), __func__)
#else
#define LOG_CALL()
#endif

//
// ConnectionContext
//

ConnectionContext::ConnectionContext() {}

ConnectionContext::~ConnectionContext() {}

void ConnectionContext::setTimeOut(unsigned long ulMillisec) const {
	LOG_CALL();
}

unsigned long ConnectionContext::getTimeOut() const {
	LOG_CALL();
	return 0;
}

void ConnectionContext::setQueue(bool bQueue) const {
	LOG_CALL();
}

bool ConnectionContext::getQueue() const {
	LOG_CALL();
	return false;
}

void ConnectionContext::connect(Timing::EventDescriptor* phEventDesc) const {
	LOG_CALL();
}

void ConnectionContext::connect(std::string strEventName) const {
	LOG_CALL();
}

void ConnectionContext::disconnect(Timing::EventDescriptor* phEventDesc) const {
	LOG_CALL();
}

void ConnectionContext::disconnect(std::string strEventName) const {
	LOG_CALL();
}

void ConnectionContext::wait(Timing::EventValue* phEventValue) const {
	LOG_CALL();

	static TimingEventScheduler tes;

	activeEvent = tes.wait();
}

bool ConnectionContext::waitAll(Timing::EventValue* phEventValue) const {
	LOG_CALL();
	return false;
}

tTimingTime ConnectionContext::getUTCTime() const {
	struct timespec ts;
	timespec_get(&ts, TIME_UTC);

	tTimingTime ttt;
	ttt.time = ts;
	return ttt;
}

unsigned long ConnectionContext::getQueueSize() const {
	LOG_CALL();
	return 0;
}

//
// EventValue
//

void EventValue::checkThisIsValid() const {
	LOG_CALL();
}

EventValue::EventValue() {
	LOG_CALL();
}

EventValue::~EventValue() {
	LOG_CALL();
}

std::string EventValue::getName() const {
	LOG_CALL();
	return activeEvent.name;
}

std::string EventValue::getName() {
	LOG_CALL();
	return activeEvent.name;
}

std::string EventValue::getNameNoCheck() const {
	LOG_CALL();
	return activeEvent.name;
}

Timing::EventDescriptor* EventValue::getEventDesc() const {
	LOG_CALL();
	return nullptr;
}

Timing::EventDescriptor* EventValue::getEventDesc() {
	LOG_CALL();
	return nullptr;
}

void EventValue::getFieldValue(Timing::Field* phField, Timing::Value* phValue) const {
	LOG_CALL();
}

void EventValue::getFieldValue(std::string strFieldName, Timing::Value* phValue) {
	LOG_CALL();
}

tTimingTime EventValue::getHwTimestamp() const {
	LOG_CALL();
	return activeEvent.hwTimestamp;
}

tTimingTime EventValue::getCycleTimestamp() const {
	LOG_CALL();
	return activeEvent.cycleTimestamp;
}

tTimingArrival EventValue::getArrival() const {
	LOG_CALL();
	return {};
}

int64_t EventValue::getCycleTime() const {
	LOG_CALL();
	return 0;
}

void EventValue::reset() {
	LOG_CALL();
}

bool EventValue::isValid() const {
	LOG_CALL();
	return true;
}

//
// Domain
//

void Domain::Domain_common() {
	LOG_CALL();
}

Domain::Domain() {
	LOG_CALL();
}

Domain::Domain(std::string strDomainName) {
	LOG_CALL();
}

Domain::~Domain() {
	LOG_CALL();
}

std::string Domain::getName() const {
	LOG_CALL();
	return activeEvent.domain;
}

void Domain::getField(std::string strFieldName, Field* phField) const {
	LOG_CALL();
}

std::vector<Timing::Field*> Domain::getFieldList() const {
	LOG_CALL();
	return {};
}

std::vector<Timing::EventDescriptor*> Domain::getEventDescList() const {
	LOG_CALL();
	return {};
}

void Domain::getFieldValueStore(tTimingTime hTimestamp, Timing::FieldValueStore* phStore) const {
	LOG_CALL();
}

void Domain::getFieldValueStoreLast(Timing::FieldValueStore* phStore) const {
	LOG_CALL();
}

void Domain::getCurrentFieldValueStore(Timing::FieldValueStore* phStore) const {
	LOG_CALL();
}

std::vector<Timing::Domain*> Domain::getList() {
	LOG_CALL();
	return {};
}

unsigned short Domain::getNumber() {
	LOG_CALL();
	return 0;
}

//
// EventDescriptor
//

void EventDescriptor::EventDescriptor_common() {
	LOG_CALL();
}

EventDescriptor::EventDescriptor() {
	LOG_CALL();
}

EventDescriptor::EventDescriptor(std::string strName) {
	LOG_CALL();
}

EventDescriptor::~EventDescriptor() {
	LOG_CALL();
}

std::string EventDescriptor::getName() const {
	LOG_CALL();
	return activeEvent.name;
}

tTimingEventClass EventDescriptor::getClass() const {
	LOG_CALL();
	return {};
}

std::vector<Timing::Field*> EventDescriptor::getFieldList() const {
	LOG_CALL();
	return {};
}

void EventDescriptor::getField(std::string strFieldName, Timing::Field* phField) const {
	LOG_CALL();
}

Timing::Domain* EventDescriptor::getDomain() const {
	LOG_CALL();
	return new Domain("Dont expect to see this");
}

std::vector<Timing::EventDescriptor*> EventDescriptor::getList() {
	LOG_CALL();
	return {};
}

//
// Value
//

void Value::checkThisIsValid() const {
	LOG_CALL();
}

Value::Value() {
	LOG_CALL();
}

Value::~Value() {
	LOG_CALL();
}

tTimingFieldType Value::getType() const {
	LOG_CALL();
	return {};
}

bool Value::getAsBool() const {
	LOG_CALL();
	return true;
}

unsigned char Value::getAsUchar() const {
	LOG_CALL();
	return 'e';
}

unsigned short Value::getAsUshort() const {
	LOG_CALL();
	return 0xdead;
}

unsigned short Value::getAsUshort() {
	LOG_CALL();
	return 0xdead;
}

int32_t Value::getAsLong32() const {
	LOG_CALL();
	return 0xdeadbabe;
}

int64_t Value::getAsLong64() const {
	LOG_CALL();
	return 0xdeadbabedeadc0deLL;
}

float Value::getAsFloat() const {
	LOG_CALL();
	return 3.1415926f;
}

double Value::getAsDouble() const {
	LOG_CALL();
	return 3.1415926;
}

std::string Value::getAsName() const {
	LOG_CALL();
	return activeEvent.user;
}

std::string Value::getAsString() const {
	LOG_CALL();
	return activeEvent.user;
}

void Value::reset() {
	LOG_CALL();
}

bool Value::isValid() const {
	LOG_CALL();
	return true;
}

//
// Field
//

void Field::Field_common() {
	LOG_CALL();
}

void Field::checkThisIsValid() const {
	LOG_CALL();
}

Field::Field() {
	LOG_CALL();
}

Field::Field(std::string strCompleteName) {
	LOG_CALL();
}

Field::~Field() {
	LOG_CALL();
}

std::string Field::getName() const {
	LOG_CALL();
	return "MyField";
}

tTimingFieldType Field::getType() const {
	LOG_CALL();
	return {};
}

void Field::getMin(Timing::Value* phValue) const {
	LOG_CALL();
}

void Field::getMax(Timing::Value* phValue) const {
	LOG_CALL();
}

void Field::getDefault(Timing::Value* phValue) const {
	LOG_CALL();
}

void Field::getLabel(std::string strLabelName, Label* phLabel) const {
	LOG_CALL();
}

std::vector<Timing::Label*> Field::getLabelList() const {
	LOG_CALL();
	return {};
}

void Field::reset() {
	LOG_CALL();
}

bool Field::isValid() const {
	LOG_CALL();
	return true;
}

}  // namespace Timing
